document.addEventListener('DOMContentLoaded', function() {
    const speakButton = document.getElementById('speak-btn');
    const textInput = document.getElementById('text-input');

    speakButton.addEventListener('click', function() {
        const text = textInput.value;
        speakText(text);
    });

    function speakText(text) {
        if ('speechSynthesis' in window) {
            const speech = new SpeechSynthesisUtterance(text);
            speech.lang = 'en-US';
            window.speechSynthesis.speak(speech);
        } else {
            alert('Sorry, your browser does not support text to speech!');
        }
    }
});
